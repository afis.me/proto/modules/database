// @generated by protobuf-ts 2.9.1
// @generated from protobuf file "database.proto" (package "database", syntax proto3)
// tslint:disable
import type { BinaryWriteOptions } from "@protobuf-ts/runtime";
import type { IBinaryWriter } from "@protobuf-ts/runtime";
import { WireType } from "@protobuf-ts/runtime";
import type { BinaryReadOptions } from "@protobuf-ts/runtime";
import type { IBinaryReader } from "@protobuf-ts/runtime";
import { UnknownFieldHandler } from "@protobuf-ts/runtime";
import type { PartialMessage } from "@protobuf-ts/runtime";
import { reflectionMergePartial } from "@protobuf-ts/runtime";
import { MESSAGE_TYPE } from "@protobuf-ts/runtime";
import { MessageType } from "@protobuf-ts/runtime";
import { Any } from "./google/protobuf/any";
/**
 * @generated from protobuf message database.SendData
 */
export interface SendData {
    /**
     * @generated from protobuf field: string ID = 1 [json_name = "ID"];
     */
    iD: string;
    /**
     * @generated from protobuf field: google.protobuf.Any Data = 2 [json_name = "Data"];
     */
    data?: Any;
}
// @generated message type with reflection information, may provide speed optimized methods
class SendData$Type extends MessageType<SendData> {
    constructor() {
        super("database.SendData", [
            { no: 1, name: "ID", kind: "scalar", jsonName: "ID", T: 9 /*ScalarType.STRING*/ },
            { no: 2, name: "Data", kind: "message", jsonName: "Data", T: () => Any }
        ]);
    }
    create(value?: PartialMessage<SendData>): SendData {
        const message = { iD: "" };
        globalThis.Object.defineProperty(message, MESSAGE_TYPE, { enumerable: false, value: this });
        if (value !== undefined)
            reflectionMergePartial<SendData>(this, message, value);
        return message;
    }
    internalBinaryRead(reader: IBinaryReader, length: number, options: BinaryReadOptions, target?: SendData): SendData {
        let message = target ?? this.create(), end = reader.pos + length;
        while (reader.pos < end) {
            let [fieldNo, wireType] = reader.tag();
            switch (fieldNo) {
                case /* string ID = 1 [json_name = "ID"];*/ 1:
                    message.iD = reader.string();
                    break;
                case /* google.protobuf.Any Data = 2 [json_name = "Data"];*/ 2:
                    message.data = Any.internalBinaryRead(reader, reader.uint32(), options, message.data);
                    break;
                default:
                    let u = options.readUnknownField;
                    if (u === "throw")
                        throw new globalThis.Error(`Unknown field ${fieldNo} (wire type ${wireType}) for ${this.typeName}`);
                    let d = reader.skip(wireType);
                    if (u !== false)
                        (u === true ? UnknownFieldHandler.onRead : u)(this.typeName, message, fieldNo, wireType, d);
            }
        }
        return message;
    }
    internalBinaryWrite(message: SendData, writer: IBinaryWriter, options: BinaryWriteOptions): IBinaryWriter {
        /* string ID = 1 [json_name = "ID"]; */
        if (message.iD !== "")
            writer.tag(1, WireType.LengthDelimited).string(message.iD);
        /* google.protobuf.Any Data = 2 [json_name = "Data"]; */
        if (message.data)
            Any.internalBinaryWrite(message.data, writer.tag(2, WireType.LengthDelimited).fork(), options).join();
        let u = options.writeUnknownFields;
        if (u !== false)
            (u == true ? UnknownFieldHandler.onWrite : u)(this.typeName, message, writer);
        return writer;
    }
}
/**
 * @generated MessageType for protobuf message database.SendData
 */
export const SendData = new SendData$Type();
